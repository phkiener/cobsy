/*
 * Copyright (c) 2018-2019 Philipp Kiener
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

#include <algorithm>
#include <cstddef>
#include <exception>
#include <iterator>

namespace cobsy {

    /**
     * Encodes the byte sequence [/start/; /end/) and writes it to /dest/.
     *
     * Encoding using COBS makes sure that no /0x00/ is contained in the
     * encoded data, making it available as packet delimiter.
     *
     * @param start start of the byte sequence to encode
     * @param end   past-the-end of the byte sequence to encode
     * @param dest  destination to write the encoded sequence to
     * @return      past-the-end of the written sequence
     */
    template <class InputIt, class OutputIt>
    OutputIt encode(InputIt start, InputIt end, OutputIt dest);

    /**
     * Decodes the byte sequence [/start/;/end/) and writes it to /dest/.
     *
     * Decoding a COBS'd sequence puts the removed /0x00/s back to their
     * original locations.
     *
     * @param start start of the byte sequence to decode
     * @param end   past-the-end of the byte sequence to decode
     * @param dest  destination to write the decoded sequence to
     * @return      past-the-end of the written sequence
     * @throw       when the data cannot be decoded due to missing or wrong bytes
     */
    template <class InputIt, class OutputIt>
    OutputIt decode(InputIt start, InputIt end, OutputIt dest);

    namespace constants {
        /**
         * The byte to escape/encpde.
         */
        constexpr uint8_t ESCAPE = 0x00;

        /**
         * Maximum number of bytes in a COBS-block.
         */
        constexpr uint8_t MAX_BLOCK_LENGTH = 254;

        /**
         * Codepoint to signal "no escaped byte in this block".
         */
        constexpr uint8_t NO_ESCAPE_IN_BLOCK = 0xFF;
    }

    /**
     * Encodes the data in /in/ and writes it to /out/.
     *
     * /in/ is accessed via /std::cbegin/ and /std::cend/, /dest/ is accessed
     * via /std::back_inserter/.
     * 
     * @see cobsy::encode(InputIt start, InputIt end, OutputIt dest)
     */
    template <class Container>
    Container& encode(const Container& in, Container& out);

    /**
     * Decodes the data in /in/ and writes it to /out/.
     *
     * /in/ is accessed via /std::cbegin/ and /std::cend/, /dest/ is accessed
     * via /std::back_inserter/.
     * 
     * @see cobsy::decode(InputIt start, InputIt end, OutputIt dest)
     */
    template <class Container>
    Container& decode(const Container& in, Container& out);

    /**
     * Encodes the data in /in/ and returns a new container.
     *
     * /in/ is accessed via /std::cbegin/ and /std::cend/. A new object of type
     * /Container/ will be default-constructed and accessed via 
     * /std::back_inserter/.
     * 
     * @see cobsy::encode(InputIt start, InputIt end, OutputIt dest)
     */
    template <class Container>
    Container encode(const Container& in);

    /**
     * Decodes the data in /in/ and returns a new container.
     *
     * /in/ is accessed via /std::cbegin/ and /std::cend/. A new object of type
     * /Container/ will be default-constructed and accessed via 
     * /std::back_inserter/.
     * 
     * @see cobsy::decode(InputIt start, InputIt end, OutputIt dest)
     */
    template <class Container>
    Container decode(const Container& in);

    namespace framed {

        /**
         * Encodes the byte sequence [/start/; /end/) and writes it to /dest/.
         *
         * Encoding using COBS makes sure that no /0x00/ is contained in the
         * encoded data, making it available as packet delimiter. A terminating
         * zero is written after the encoded bytes automatically.
         *
         * @param start start of the byte sequence to encode
         * @param end   past-the-end of the byte sequence to encode
         * @param dest  destination to write the encoded sequence to
         * @return      past-the-end of the written sequence
         */
        template <class InputIt, class OutputIt>
        OutputIt encode(InputIt start, InputIt end, OutputIt dest);

        /**
         * Decodes the byte sequence [/start/;) and writes it to /dest/.
         * 
         * Decoding a COBS'd sequence puts the removed /0x00/s back to their
         * original locations. The end of the sequence is determined once a
         * zero byte is encountered.
         * 
         * Warning: This will repeatedly call std::find to determine a zero
         *
         * @param start start of the byte sequence to decode
         * @param dest  destination to write the decoded sequence to
         * @return      past-the-end of the written sequence
         * @throw       when the data cannot be decoded due to missing or wrong bytes
         */
        template <class InputIt, class OutputIt>
        OutputIt decode(InputIt start, OutputIt dest);
    }

    namespace detail {

        /**
         * Decodes the byte sequence [/start/;) and writes it to /dest/.
         *
         * Decoding a COBS'd sequence puts the removed /0x00/s back to their
         * original locations. The sequence is read until /end_condition/ returns
         * true when called with an iterator.
         *
         * @param start         start of the byte sequence to decode
         * @param end_condition predicate to determine end of input
         * @param dest          destination to write the decoded sequence to
         * @return              past-the-end of the written sequence
         * @throw               when the data cannot be decoded due to missing or wrong bytes
         */
        template <class InputIt, class OutputIt, class UnaryPredicate>
        OutputIt decode(InputIt start, UnaryPredicate end_condition, OutputIt dest);
    }
}

// ----------------------------------------------------------------------------------------------------------------- //
//                                                  Implementation                                                   //
// ----------------------------------------------------------------------------------------------------------------- //

template <class InputIt, class OutputIt>
inline OutputIt cobsy::encode(InputIt start, InputIt end, OutputIt dest) {
    using namespace cobsy::constants;

    auto it = start;

    std::vector<std::uint8_t> buf;
    buf.reserve(254);

    while (it != end) {
        std::uint8_t dist = 0;
        std::uint8_t current = ESCAPE;

        while (dist < MAX_BLOCK_LENGTH && it != end) {
            current = *it++;

            if (current == ESCAPE) {
                break;
            } else {
                buf.push_back(current);
                dist += 1;
            }
        }

        *dest++ = dist + 1;
        std::copy(buf.cbegin(), buf.cend(), dest);
        buf.clear();

        if (current == ESCAPE && it == end) {
            *dest++ = 1;
        }
    }

    return dest;
}

template <class InputIt, class OutputIt>
inline OutputIt cobsy::decode(InputIt start, InputIt end, OutputIt dest) {
    auto at_end = [&end] (auto it) { return it == end; };
    return cobsy::detail::decode(start, at_end, dest);
}

template <class Container>
inline Container& cobsy::encode(const Container& in, Container& out) {
    encode(std::cbegin(in), std::cend(in), std::back_inserter(out));
    return out;
}

template <class Container>
inline Container& cobsy::decode(const Container& in, Container& out) {
    decode(std::cbegin(in), std::cend(in), std::back_inserter(out));
    return out;
}

template <class Container>
inline Container cobsy::encode(const Container& in) {
    Container result;
    return encode(in, result);
}

template <class Container>
inline Container cobsy::decode(const Container& in) {
    Container result;
    return decode(in, result);
}
        
template <class InputIt, class OutputIt>
inline OutputIt cobsy::framed::encode(InputIt start, InputIt end, OutputIt dest) {
    auto res = cobsy::encode(start, end, dest);
    *res++ = cobsy::constants::ESCAPE;

    return res;
}

template <class InputIt, class OutputIt>
inline OutputIt cobsy::framed::decode(InputIt start, OutputIt dest) {
    auto end = [] (auto it) { return *it == cobsy::constants::ESCAPE; };
    return cobsy::detail::decode(start, end, dest);
}

template <class InputIt, class OutputIt, class UnaryPredicate>
inline OutputIt cobsy::detail::decode(InputIt start, UnaryPredicate end_condition, OutputIt dest) {
    using namespace cobsy::constants;

    auto it = start;

    while (!end_condition(it)) {
        std::uint8_t count = *it++;

        for (auto i = 1; i < count; ++i) {
            if (end_condition(it)) {
                throw std::runtime_error("Error while decoding data");
            }

            *dest++ = *it++;
        }

        if (count != NO_ESCAPE_IN_BLOCK && !end_condition(it)) {
            *dest++ = ESCAPE;
        }
    }

    return dest;
}