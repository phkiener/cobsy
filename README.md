# cobsy - Header-only, Iterator-based, modern COBS

COBS is short for Consistent Overhead Byte Stuffing and is a way of translating
byte sequences with bytes [0;255] to byte sequences with bytes [1;255] while
using minimal overhead. This is useful if you want to terminate packets using
a zero-byte, which mandates it being escaped in the data. COBS can do that!

## Building

No building required for `cobsy` - it is a single header.
There is an example you can build and run, though. To do that, use:

    mkdir build
    cd build
    cmake .. -DCOBSY_BUILD_EXAMPLE=true

And you're good to go.

There's also a test suite shipped with `cobsy`; you can enable it by passing
`-DCOBSY_BUILD_TESTS=true` to the `cmake`-call.

## Installation

Just drop `cobsy.h` somewhere in your project and include it. Done!

## Usage

In namespace `cobsy`, there are two functions `encode` and `decode`.

- `encode` takes in a sequence and encodes that sequence using COBS
- `decode` takes in a COBS-encoded sequence and recreates the original sequence

See the provided `main.cpp` in `src/` for an example.
