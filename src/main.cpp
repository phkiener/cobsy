// Copyright (c) 2018-2019 Philipp Kiener

#include <cstdint>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <cobsy.h>

enum direction { encode, decode };

void call_cobsy(std::istream& in, std::ostream& out, direction d) {
    auto i = std::istream_iterator<std::uint8_t>(in);
    auto o = std::ostream_iterator<std::uint8_t>(out);

    if (d == encode) {
        cobsy::encode(i, std::istream_iterator<std::uint8_t>(), o);
    } else {
        cobsy::decode(i, std::istream_iterator<std::uint8_t>(), o);
    }
}

int main(int argc, const char** argv) {

    bool show_help = false;
    direction dir = encode;
    std::string file = "-";

    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];

        if (arg == "-h" || arg == "--help") {
            show_help = true;
        } else if (arg == "-d" || arg == "--decode") {
            dir = decode;
        } else if (arg == "-e" || arg == "--encode") {
            dir = encode;
        } else if (arg[0] == '-') {
            std::cerr
                << "Unrecognized option '" << arg << "'\n"
                << "Type '" << argv[0] << " --help' for usage.\n"
                ;
        } else {
            file = arg;
            break;
        }
    }

    if (show_help) {
        std::cout
            << "Usage: " << argv[0] << " [OPTIONS] [FILE]\n"
            << "\n"
            << "OPTIONS:\n"
            << "    -h | --help    : Print this help text\n"
            << "    -d | --decode  : Decode the input\n"
            << "    -e | --encode  : Encode the input (default)\n"
            << "\n"
            << "FILE:\n"
            << "    The file to read from. If no FILE is given, stdin is read instead.\n"
            ;
        return 0;
    }

    if (file == "-") {
        std::cin.unsetf(std::ios_base::skipws);
        call_cobsy(std::cin, std::cout, dir);
    } else {
        std::ifstream f(file);

        if (!f.good()) {
            std::cerr << "Cannot open file '" << file << "'\n";
            return 1;
        }

        f.unsetf(std::ios_base::skipws);
        call_cobsy(f, std::cout, dir);
    }

    return 0;
}
