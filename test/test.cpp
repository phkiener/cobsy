// Copyright (c) 2018-2019 Philipp Kiener

#include "catch.hpp"

#include <cobsy.h>

#include <algorithm>
#include <chrono>
#include <cstddef>
#include <iterator>
#include <random>
#include <vector>

std::vector<uint8_t> encode(const std::vector<uint8_t>& bytes) {
    std::vector<uint8_t> result;

    cobsy::encode(bytes.cbegin(), bytes.cend(), std::back_inserter(result));
    return result;
}

std::vector<uint8_t> decode(const std::vector<uint8_t>& bytes) {
    std::vector<uint8_t> result;

    cobsy::decode(bytes.cbegin(), bytes.cend(), std::back_inserter(result));
    return result;
}

TEST_CASE("encoding with zeroes", "[encode]") {
    std::vector<uint8_t> source;
    std::vector<uint8_t> target;

    SECTION("single zero") {
        source = { 0x00 };
        target = { 0x01, 0x01};

        REQUIRE(target == encode(source));
    }

    SECTION("multiple zeroes") {
        source = { 0x00, 0x00 };
        target = { 0x01, 0x01, 0x01 };

        REQUIRE(target == encode(source));
    }

    SECTION("one zero at start, rest non-zero") {
        source = { 0x00, 0x11, 0x22, 0x33 };
        target = { 0x01, 0x04, 0x11, 0x22, 0x33 };

        REQUIRE(target == encode(source));
    }

    SECTION("one zero at end, rest non-zero") {
        source = { 0x11, 0x22, 0x33, 0x00 };
        target = { 0x04, 0x11, 0x22, 0x33, 0x01 };

        REQUIRE(target == encode(source));
    }

    SECTION("one zero in mid, rest non-zero") {
        source = { 0x11, 0x22, 0x00, 0x33 };
        target = { 0x03, 0x11, 0x22, 0x02, 0x33 };

        REQUIRE(target == encode(source));
    }

    SECTION("multiple zeroes at start, rest non-zero") {
        source = { 0x00, 0x00, 0x11, 0x22 };
        target = { 0x01, 0x01, 0x03, 0x11, 0x22 };

        REQUIRE(target == encode(source));
    }

    SECTION("multiple zeroes at end, rest non-zero") {
        source = { 0x11, 0x22, 0x00, 0x00 };
        target = { 0x03, 0x11, 0x22, 0x01, 0x01 };

        REQUIRE(target == encode(source));
    }

    SECTION("multiple zeroes in mid (chunk), rest non-zero") {
        source = { 0x11, 0x00, 0x00, 0x22 };
        target = { 0x02, 0x11, 0x01, 0x02, 0x22 };

        REQUIRE(target == encode(source));
    }
    SECTION("multiple zeroes in mid (diffuse), rest non-zero") {
        source = { 0x11, 0x00, 0x22, 0x00, 0x33 };
        target = { 0x02, 0x11, 0x02, 0x22, 0x02, 0x33 };

        REQUIRE(target == encode(source));
    }
}

TEST_CASE("encoding without zeroes", "[encode]") {
    std::vector<uint8_t> source;
    std::vector<uint8_t> target;

    SECTION("single non-zero") {
        source = { 0x11 };
        target = { 0x02, 0x11};

        REQUIRE(target == encode(source));
    }

    SECTION("multiple non-zeroes") {
        source = { 0x11, 0x22 };
        target = { 0x03, 0x11, 0x22 };

        REQUIRE(target == encode(source));
    }

    SECTION("more than 254 non-zeroes") {
        std::fill_n(std::back_inserter(source), 1000, 0x01);

        // 0 bytes
        target.push_back(0xFF);
        target.insert(target.end(), 254, 0x01);

        // 254 bytes
        target.push_back(0xFF);
        target.insert(target.end(), 254, 0x01);

        // 508 bytes
        target.push_back(0xFF);
        target.insert(target.end(), 254, 0x01);

        // 762 bytes
        target.push_back(0xEF);
        target.insert(target.end(), 238, 0x01);

        // 1000 bytes

        REQUIRE(target == encode(source));
    }
}

TEST_CASE("decode without zeroes in result", "[decode]") {
    std::vector<uint8_t> source;
    std::vector<uint8_t> target;

    SECTION("single non-zero") {
        source = { 0x02, 0x11};
        target = { 0x11 };

        REQUIRE(target == decode(source));
    }

    SECTION("multiple non-zeroes") {
        source = { 0x03, 0x11, 0x22 };
        target = { 0x11, 0x22 };

        REQUIRE(target == decode(source));
    }

    SECTION("more than 254 non-zeroes") {
        // 0 bytes
        source.push_back(0xFF);
        source.insert(source.end(), 254, 0x01);

        // 254 bytes
        source.push_back(0xFF);
        source.insert(source.end(), 254, 0x01);

        // 508 bytes
        source.push_back(0xFF);
        source.insert(source.end(), 254, 0x01);

        // 762 bytes
        source.push_back(0xEF);
        source.insert(source.end(), 238, 0x01);

        // 1000 bytes

        std::fill_n(std::back_inserter(target), 1000, 0x01);

        REQUIRE(target == decode(source));
    }
}

TEST_CASE("decode with zeroes in result", "[decode") {
    std::vector<uint8_t> source;
    std::vector<uint8_t> target;

    SECTION("single zero") {
        source = { 0x01, 0x01};
        target = { 0x00 };

        REQUIRE(target == decode(source));
    }

    SECTION("multiple zeroes") {
        source = { 0x01, 0x01, 0x01 };
        target = { 0x00, 0x00 };

        REQUIRE(target == decode(source));
    }

    SECTION("one zero at start, rest non-zero") {
        source = { 0x01, 0x04, 0x11, 0x22, 0x33 };
        target = { 0x00, 0x11, 0x22, 0x33 };

        REQUIRE(target == decode(source));
    }

    SECTION("one zero at end, rest non-zero") {
        source = { 0x04, 0x11, 0x22, 0x33, 0x01 };
        target = { 0x11, 0x22, 0x33, 0x00 };

        REQUIRE(target == decode(source));
    }

    SECTION("one zero in mid, rest non-zero") {
        source = { 0x03, 0x11, 0x22, 0x02, 0x33 };
        target = { 0x11, 0x22, 0x00, 0x33 };

        REQUIRE(target == decode(source));
    }

    SECTION("multiple zeroes at start, rest non-zero") {
        source = { 0x01, 0x01, 0x03, 0x11, 0x22 };
        target = { 0x00, 0x00, 0x11, 0x22 };

        REQUIRE(target == decode(source));
    }

    SECTION("multiple zeroes at end, rest non-zero") {
        source = { 0x03, 0x11, 0x22, 0x01, 0x01 };
        target = { 0x11, 0x22, 0x00, 0x00 };

        REQUIRE(target == decode(source));
    }

    SECTION("multiple zeroes in mid (chunk), rest non-zero") {
        source = { 0x02, 0x11, 0x01, 0x02, 0x22 };
        target = { 0x11, 0x00, 0x00, 0x22 };

        REQUIRE(target == decode(source));
    }
    SECTION("multiple zeroes in mid (diffuse), rest non-zero") {
        source = { 0x02, 0x11, 0x02, 0x22, 0x02, 0x33 };
        target = { 0x11, 0x00, 0x22, 0x00, 0x33 };

        REQUIRE(target == decode(source));
    }
}

TEST_CASE("decode on faulty data", "[decode]") {
    std::vector<uint8_t> source = { 0x02, 0x11, 0x02, 0x22, 0x02, 0x33 };
    std::vector<uint8_t> target = { 0x11, 0x00, 0x22, 0x00, 0x33 };
    std::vector<uint8_t> decoded;

    SECTION("Missing bytes at start") {
        decoded.clear();
        REQUIRE_THROWS(cobsy::decode(source.begin() + 1, source.end(), std::back_inserter(decoded)));
    }

    SECTION("Missing bytes in mid") {
        decoded.clear();
        REQUIRE_THROWS(cobsy::decode(source.begin() + 1, source.end(), std::back_inserter(decoded)));
    }

    SECTION("Missing bytes at end") {
        decoded.clear();
        REQUIRE_THROWS(cobsy::decode(source.begin() + 1, source.end(), std::back_inserter(decoded)));
    }

    SECTION("Byte errors") {
        auto sourceJumbled = source;
        sourceJumbled[3] += 10;

        decoded.clear();
        REQUIRE_THROWS(cobsy::decode(sourceJumbled.begin() + 1, sourceJumbled.end(), std::back_inserter(decoded)));
    }
}

TEST_CASE("decode(encode(source)) yields source", "") {
    SECTION("Handcoded data") {
        std::vector<std::vector<uint8_t>> tests;

        tests.emplace_back(std::vector<uint8_t>({0x00, 0x01, 0x02}));
        tests.emplace_back(std::vector<uint8_t>({0x01, 0x02, 0x00}));
        tests.emplace_back(std::vector<uint8_t>({0x01, 0x00, 0x02}));
        tests.emplace_back(std::vector<uint8_t>({0x11, 0x00, 0x00, 0x22, 0x00, 0x00, 0x33}));

        {
            std::vector<uint8_t> thousandOnes;
            std::fill_n(std::back_inserter(thousandOnes), 1000, 0x01);

            tests.emplace_back(std::move(thousandOnes));
        }

        for (const auto& test : tests) {
            REQUIRE(test == decode(encode(test)));
        }
    }
}
